import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.stream.IntStream;
public class Test {
    public static String searchedHash = "ad35bb9134a00319d0c34e5cd78e9348a6655660";
    public static String sha1(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
            return org.apache.commons.codec.binary.Hex.encodeHexString(hash);
        } catch (Exception x) {
            throw new RuntimeException(x);
        }

    }
    public static void main(String[] args){
        Test test = new Test();
        IntStream.range(0,1000000)
                .parallel()
                .mapToObj(value -> {
                    String str = formatInteger(value);
                    return str;
                })
                .map(value -> {
                    HashMap<String, String> map = new HashMap<>();
                    map.put(sha1(value), value);
                    return map;
                })
                .filter(value -> value.get(searchedHash) != null)
                .findFirst()
                .ifPresent(s -> System.out.println(s.get(searchedHash))
                );
    }
    public static String formatInteger(int value){
        return String.format("%06d", value);
    }
}

